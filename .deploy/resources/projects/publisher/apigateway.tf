resource "aws_api_gateway_rest_api" "lambda-api" {
  name        = "${local.project_name}"
  description = "Terraform API Gateway for training - ${var.env_name}"
  policy      = "${data.template_file.api_access.rendered}"
  endpoint_configuration {
      types = ["REGIONAL"]
    }
}

data template_file "api_access" {
  template = "${file("files/policy/api_gw.json")}"
}

data "aws_iam_policy_document" "api_access"{
  statement {
    actions = [
      "execute-api:Invoke"
    ]

    resources = [
      "execute-api:/*/GET/*"
    ]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
  }
}

resource "aws_api_gateway_integration" "lambda_root" {
  rest_api_id = "${aws_api_gateway_rest_api.lambda-api.id}"
  resource_id = "${aws_api_gateway_method.proxy_root.resource_id}"
  http_method = "${aws_api_gateway_method.proxy_root.http_method}"

  # Lambda function can only be invoked via POST
  # integration_http_method has to be set to POST 
  # https://www.terraform.io/docs/providers/aws/r/api_gateway_integration.html
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "${module.hello_world_lambda.invoke_arn}"
}

resource "aws_api_gateway_deployment" "lambda-api" {
  depends_on = [
    "aws_api_gateway_integration.lambda_root"
  ]

  rest_api_id = "${aws_api_gateway_rest_api.lambda-api.id}"
  stage_name  = "deployment"
}

resource "aws_api_gateway_stage" "lambda-api" {
  depends_on = [
    "aws_api_gateway_rest_api.lambda-api",
    "aws_cloudwatch_log_group.api_gw_log_group"
  ]

  stage_name           = "${var.env_name}"
  rest_api_id          = "${aws_api_gateway_rest_api.lambda-api.id}"
  deployment_id        = "${aws_api_gateway_deployment.lambda-api.id}"
  xray_tracing_enabled = true
  tags = {
   name        = "${local.full_api_name}"
   environment = "${var.env_name}"
   source      = "${local.source}"
   service     = "${local.service_name}"
   team        = "${local.team}"
   technology  = "apigateway"
 }
 access_log_settings {
    destination_arn    = "arn:aws:logs:${var.aws_region}:${var.account_id}:log-group:API-Gateway-Execution-Logs_${aws_api_gateway_rest_api.lambda-api.id}/${var.env_name}:*"
    format             = "${file("files/log_format.json")}" 
  }

}

resource "aws_cloudwatch_log_group" "api_gw_log_group" {
  name = "API-Gateway-Execution-Logs_${aws_api_gateway_rest_api.lambda-api.id}/${var.env_name}"

  tags = {
    Name        = "${local.full_api_name}"
    Environment = "${var.env_name}"
    Source      = "${local.source}"
    Service     = "${local.service_name}"
    Team        = "${local.team}"
    Technology  = "apigateway"
  }
}


resource "aws_api_gateway_method" "proxy_root" {
  rest_api_id   = "${aws_api_gateway_rest_api.lambda-api.id}"
  resource_id   = "${aws_api_gateway_rest_api.lambda-api.root_resource_id}"
  http_method   = "GET"
  authorization = "NONE"
  api_key_required ="false"
}

resource "aws_api_gateway_method_response" "200" {
  rest_api_id = "${aws_api_gateway_rest_api.lambda-api.id}"
  resource_id = "${aws_api_gateway_rest_api.lambda-api.root_resource_id}"
  http_method = "${aws_api_gateway_method.proxy_root.http_method}"
  status_code = "200"
}
resource "aws_api_gateway_method_response" "502" {
  rest_api_id = "${aws_api_gateway_rest_api.lambda-api.id}"
  resource_id = "${aws_api_gateway_rest_api.lambda-api.root_resource_id}"
  http_method = "${aws_api_gateway_method.proxy_root.http_method}"
  status_code = "502"
}
resource "aws_api_gateway_method_response" "404" {
  rest_api_id = "${aws_api_gateway_rest_api.lambda-api.id}"
  resource_id = "${aws_api_gateway_rest_api.lambda-api.root_resource_id}"
  http_method = "${aws_api_gateway_method.proxy_root.http_method}"
  status_code = "404"
}
resource "aws_lambda_permission" "apigw_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${module.hello_world_lambda.arn}"
  principal     = "apigateway.amazonaws.com"

  # The /*/* portion grants access from any method on any resource
  # within the API Gateway "REST API".
  source_arn = "${aws_api_gateway_rest_api.lambda-api.execution_arn}/*/${aws_api_gateway_method.proxy_root.http_method}/"
}
