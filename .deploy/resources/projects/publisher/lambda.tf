locals {
  function_name      = "${local.project_name}-${var.env_name}"
  filename_gw_lambda = "index"
}

data "archive_file" "lambda_archive" {
  type        = "zip"
  output_path = "${path.module}/index.zip"
  source_dir  = "${path.module}/dist"
}

data "template_file" "lambda_policy" {
  template = "${file("files/policy/lambda.json")}"

  vars {
    account_id    = "${var.account_id}"
    aws_region    = "${var.aws_region}"
    function_name = "${local.function_name}"
    }
}

module "hello_world_lambda" {
  source          = "git::ssh://git@gitlab.com/rs-devsecops/rs-ccc/terraform-modules//aws/compute/lambda?ref=0.0.139"
  code_file       = ""
  env_name        = "${var.env_name}"
  event_driven    = 0
  filename        = "${local.filename_gw_lambda}"
  function_name   = "${local.function_name}"
  lambda_role     = "${file("files/roles/assume-role-lambda.json")}"
  policy_file     = "${data.template_file.lambda_policy.rendered}"
  project_name    = "${local.project_name}"
  runtime         = "nodejs10.x"
  timeout         = "${var.lambda_timeout_seconds}"
  zip_path        = "${data.archive_file.lambda_archive.output_path}"
  memory_size     = "${var.lambda_memory_size}"
  # tracing_config  = "${var.tracing_config}"

  variables = {
    environment     = "${var.env_name}"
  }

  tags = {
    Source      = "${local.source}"
    Service     = "${local.service_name}"
    Team        = "${local.team}"
    Name        = "${local.function_name}"
  }
}

# resource "aws_lambda_event_source_mapping" "example" {
#   event_source_arn = "${aws_sqs_queue.pms_price_processor_queue.arn}"
#   function_name    = "${module.pms_price_publishing_lambda.arn}"
#   batch_size       = 1
# }

output lambda_name {
  value = "${local.function_name}"  
}