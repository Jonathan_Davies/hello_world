provider "aws" {
  version = "~> 2.11"
  region = "${var.aws_region}"

  assume_role {
    role_arn = "arn:aws:iam::${var.account_id}:role/${var.sts_prefix}-${var.tfstate_env}"
  }
}

terraform {
  backend "s3" {}
}

locals {
  source                        = "training"
  team                          = "Team Jonny"
  service_name                  = "hello_world"
  project_name                  = "${local.source}-${local.service_name}"
}
