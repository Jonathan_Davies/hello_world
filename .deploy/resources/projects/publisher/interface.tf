variable account_id {}
variable aws_region {}

variable env_file {}

variable env_name {
  description = "Environment name, usually set in env.tfvars."
}

variable shared_account_id {}
variable sts_prefix {}
variable tfstate_bucket {}
variable tf_state_region {}
variable tfstate_env {}
variable max_session_duration {
  default = 3600
}
# variable dynamodb_query_timeout_millis {}
variable lambda_timeout_seconds {}
# variable route53_zone_id {}
# variable dms_document_service_domain_name {}
# variable kong_service_name_postfix {}
# variable tracing_config {}

variable lambda_memory_size {
  default = 256
}

variable lambda_log_level {
  default = "info" # debug, info, error.
}

# variable public_docs_domain_name {}

# variable dynamodb_query_retry_limit { default = 1} // one retry
# variable xray_logging_enabled { default = "false"}

data "terraform_remote_state" "cas" {
  backend = "s3"

  config {
    bucket = "${var.tfstate_bucket}"
    key    = "env:/${var.tfstate_env}/resources/iam/cross_account_signin/terraform.tfstate"
    region = "${var.aws_region}"
  }
}

# data "terraform_remote_state" "kong" {
#   backend = "s3"

#   config {
#     bucket = "${var.tfstate_bucket}"
#     key="env:/${var.tfstate_env}/resources/network/kong/terraform.tfstate"
#     region = "${var.aws_region}"
#   }
# }

# data "terraform_remote_state" "waf" {
#   backend = "s3"

#   config {
#     bucket = "${var.tfstate_bucket}"
#     key="env:/${var.tfstate_env}/resources/network/loadbalancers/terraform.tfstate"
#     region = "${var.aws_region}"
#   }
# }