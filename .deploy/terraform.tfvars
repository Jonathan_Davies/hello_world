terragrunt = {
  remote_state = {
    backend = "s3"
    config {
      encrypt = true
      bucket = "rs-cni-tfstate"
      key = "env:/${get_env("TF_VAR_env_file", "")}/${path_relative_to_include()}/terraform.tfstate"
      region = "eu-west-1"
      dynamodb_table = "rs-cni-terraform-${get_env("TF_VAR_env_file", "")}-lock"
    }
  }

  terraform = {
    extra_arguments "modules" {
      commands = ["get"]
      arguments = ["-update=true"]
    }

    extra_arguments "retry_lock" {
      commands = ["init", "apply", "refresh", "import", "plan", "taint", "untaint"]
      arguments = [
        "-lock-timeout=20m",
        "-var-file=${get_parent_tfvars_dir()}/environments/${get_env("TF_VAR_env_file", "")}/env.tfvars"
      ]
    }

    extra_arguments "destroy" {
      commands = ["destroy"]
      arguments = [
        "-var-file=${get_parent_tfvars_dir()}/environments/${get_env("TF_VAR_env_file", "")}/env.tfvars"
      ]
    }

    extra_arguments "backups" {
      commands = ["apply"]
      arguments = ["-backup=-"]
    }
  }
}
